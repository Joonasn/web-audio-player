var audio = document.getElementById("music");
var slider = document.getElementById("slider");
var volumeRange = document.getElementById("volumeSetup");
var isVisible=document.getElementById('volumeSetup');
var musicMenu = document.getElementById('changeSelection');

var getData = function(url, success, error) {  //JSON request call to get audio information
	var request = new XMLHttpRequest();

	request.onreadystatechange = function() {
		try {
			if (request.readyState === XMLHttpRequest.DONE) {
				if (request.status === 200) {
					success(JSON.parse(request.responseText));
				} else {
					if (request.status === 0) {
						error('Server response not received');
					} else {
						error('There was a problem with the request: ' + request.statusText);
					}
				}
			}
		} catch(e) {
			error(e);
		}
	};
	request.open('GET', url);
	request.send();
};

function resetPlayer() { //Reset audio data and load audio choices
	stopPress();
	volumeRange.value = "0.50";
	isVisible.style.visibility ='hidden';
	volumeToggle();
	loadChoices();
}

function loadChoices() {
	getData('http://localhost:3000/audio/', function(json) {
		json.files.forEach(function(data) {
			var option = document.createElement('option');
			option.id = "change";
			option.value = data.audioName;
			option.innerHTML = data.audioName;
			musicMenu.appendChild(option);
		});
		getAudio(musicMenu.value);

        }, function(reason) {
        	alert (reason);
	});	
}

function getAudio(choice) {
	audio.src='audio/'+ choice;
	audio.load() //Load audio based on given data
}

//Buttons 
function playPress() {
	stopPress()
	audio.play(); //Reset audio and start from the beginning
}

function pausePress() { // Handle pausing
    if (audio.paused) {
		if (audio.currentTime>0){
			audio.play();
		}
	}
	else {
		audio.pause();
	}
}

function stopPress() { //Pause audio and reset it's values
	audio.pause();
	audio.currentTime = 0;
	slider.value = audio.currentTime;
}

function skipAudio(change) {
	stopPress();
	var changeIndex = musicMenu.options.selectedIndex + change; //Change index
	musicMenu.options.selectedIndex = changeIndex;

	if (changeIndex < 0){
		musicMenu.options.selectedIndex = (musicMenu.options.length-1); // index is less than 0
	}
	else if (changeIndex > (musicMenu.options.length-1)) {  // index is less than maximum value
		musicMenu.options.selectedIndex = 0;
	}
	getAudio(musicMenu.value);
}

// Slider
var isActive = false;
function songRewinding(value) {
	isActive = value;
	if (!value) {
		audio.currentTime = slider.value; //if user isn't using range, change audio timer
		audio.play();
	}
}

function rewindUpdate(play,length) { //Change range position based on audio data
	if (!isActive) {
		slider.value = play;
	}
	slider.max = length;
}

// Volume slider
function openVolume() {
	if (isVisible.style.visibility == 'hidden') { //Change the visibility of the volume range
		isVisible.style.visibility ='visible';
	}
	else {
		isVisible.style.visibility ='hidden';
	}
}

function volumeToggle() {
	audio.volume = volumeRange.value; //Change audio volume based on range
}

// Timers
function currentlyPlaying() {
	// document.getElementById('TotalTime').innerHTML = Math.floor(audio.currentTime) + ' / ' + Math.floor(audio.duration);
	// Calculates how long the song has been played
	var playTime = audio.currentTime| 0;
	var hours = "0" + Math.floor(playTime / 3600);
	var minutes = "0" + Math.floor(playTime / 60);
    var seconds = "0" + (playTime - minutes * 60);
    var playTimeReadable = hours.substr(-2) + ":" + minutes.substr(-2) + ":" + seconds.substr(-2);
	document.getElementById('songPlayingTime').innerHTML = playTimeReadable; // Return value

	// Calculates how long the song is
	var songLength = audio.duration| 0;
    var lengthHour = "0" + Math.floor(songLength / 3600);
    var lengthMin = "0" + Math.floor(songLength / 60);
    var lengthSeconds = "0" + (songLength - lengthMin * 60);
    var songLengthReadable = "/" + lengthHour.substr(-2) + ":" + lengthMin.substr(-2) + ":" + lengthSeconds.substr(-2);
    document.getElementById('songsLength').innerHTML = songLengthReadable;

	// How much there is left of the song to finish
	var howMuchLeft = "0" + (songLength - playTime);
	var leftHour = "0" + Math.floor(howMuchLeft / 3600);
    var leftMin = "0" + Math.floor(howMuchLeft / 60);
    var leftSeconds = "0" + (howMuchLeft - leftMin* 60);
	var songLeft = "/" + leftHour.substr(-2) + ":" + leftMin.substr(-2) + ":" + leftSeconds.substr(-2);
    document.getElementById('songLeft').innerHTML = songLeft;
	rewindUpdate(playTime,songLength);
}