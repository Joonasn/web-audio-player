const express = require('express');
const app = express();
const http = require('http');
const fileSystem = require('fs');
const hostname = 'localhost';
const port = 3000;

app.use(express.static('dependencies'));
const audioFolder = './dependencies/audio';

app.get ('/audio', function(req, res){
  var audioFiles= [];
  var getAudioInfo = function(callback) {
    fileSystem.readdirSync(audioFolder).forEach(file => {
      audioFiles.push({
        "audioName":file
      })
    });
    return callback(audioFiles);
  };

  getAudioInfo(function (audioArray) {
    res.setHeader('Content-Type', 'application/json');
    res.json({"files":audioArray});
  });
});

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World');
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});

app.listen(port);